<?php
$error = '';

if (isset($_POST['param']) && is_numeric($_POST['param'])) {
    $param = $_POST['param'];
}else {
    $param = 0;
    $error = "Введите число!";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task2</title>
    <style>
        form {
            text-align: center;
        }
        input {
            display: block;
            margin: 10px auto;
            padding: 0 5px;
        }
        .result {
            width: 50%;
            margin: 50px auto;
            text-align: center;
        }
    </style>
</head>
<body>
<form action="" method="post">
    <p>Определения n-го числа Фибоначчи</p>
    <input type="text" name="param" value="<?=$param;?>">
    <?=$error;?>
    <input type="submit" value="Проверить">
</form>
</body>
</html>

<?php
if($param > 0) {
    fibonacci($param);
}


function fibonacci($num) {
    $a = 0;
    $b = 1;
    $res = 0;
    echo "<div class='result'>";

    for ($i = 0; $i < $num; $i++) {
        $res = $a + $b;
        $a = $b;
        $b = $res;
        echo $res.' ';
    }
    echo "</div>";
}
?>